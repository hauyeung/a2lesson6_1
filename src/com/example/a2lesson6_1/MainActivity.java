package com.example.a2lesson6_1;

import android.os.Bundle;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.view.Menu;
import android.view.View;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		 findViewById(R.id.button_notify_now).setOnClickListener(new View.OnClickListener() {
		      @Override
		      public void onClick(View v) {
		        notifyNow();
		      }
		    });
	}
	
	public void notifyNow() {
		Intent resultIntent = new Intent(this, NextActivity.class);
		resultIntent.putExtra("id",0);
	    PendingIntent pi = TaskStackBuilder.create(this)
	      .addParentStack(MainActivity.class)
	      .addNextIntent(resultIntent)
	      .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
	    
	    Notification notification = new NotificationCompat.Builder(this)
	      .setSmallIcon(R.drawable.ic_launcher)
	      .setContentTitle("Notify now")
	      .setContentText("You've been notified!")
	      .setContentIntent(pi)
	      .build();
	    
	    NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
	    nm.notify(resultIntent.getIntExtra("id", 0), notification);
	  }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
