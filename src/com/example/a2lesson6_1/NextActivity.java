package com.example.a2lesson6_1;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

public class NextActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    
    setContentView(R.layout.activity_next);
    
    Intent resultIntent = new Intent(this, MainActivity.class);
    PendingIntent pi = TaskStackBuilder.create(this)
        .addParentStack(this)
        .addNextIntent(resultIntent)
        .getPendingIntent(resultIntent.getIntExtra("id", 0), PendingIntent.FLAG_UPDATE_CURRENT);
    
    Notification notification = new NotificationCompat.Builder(this)
      .setSmallIcon(R.drawable.ic_launcher)
      .setContentTitle("Next Notify")
      .setContentText("You've been re-notified!")
      .setContentIntent(pi)
      .build();
    
    NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    nm.notify(resultIntent.getIntExtra("id", 0), notification);
  }
}